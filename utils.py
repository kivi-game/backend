import random

def generate_dice(count = 6, *, min = 1, max = 6) -> list:
    return [random.randint(min, max) for _ in range(count)]
