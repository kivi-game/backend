from flask import Flask
from flask_restful import Resource, Api

from utils import generate_dice
from checker import Checker

app = Flask(__name__)
api = Api(app)

class Dice(Resource):
    def get(self):
        dice = generate_dice()
        checker = Checker(dice)

        return {
            'dice': dice,
            'combinations': checker.check_all()
        }

api.add_resource(Dice, '/dice')

if __name__ == '__main__':
    app.run()
