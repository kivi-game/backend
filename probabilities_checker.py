from tqdm import tqdm
from checker import Checker
from utils import generate_dice

iterations = 100000

combinations_found = {c : 0 for c in Checker.get_supported_combinations()}

for i in tqdm(range(iterations)):
    dice = generate_dice()
    checker = Checker(dice)

    for c in checker.check_all():
        combinations_found[c] += 1

combinations_found = {c : combinations_found[c] / iterations * 100 for c in combinations_found if combinations_found[c] > 0}

for c in sorted(combinations_found, key = lambda x : combinations_found[x], reverse = True):
    print(f'{c} -> {combinations_found[c]:.2f}%')

print(f'Total: {sum(combinations_found.values()):.2f}%')
