from types import FunctionType
from typing import Dict, List, Set, Tuple, Type
from enum import IntFlag
from functools import wraps

class CheckerTypes(IntFlag):
    ONLY_VALUES = 1 << 3,
    SUM = 1 << 2,
    SEQUENCE = 1 << 1,
    PAIRS = 1 << 0,

class Checker():
    __checker_list = None
    __Static = None

    def __init__(self, dice_values : List[int], *, min_dice : int = 1, max_dice : int = 6, 
    checker_type : CheckerTypes = CheckerTypes.ONLY_VALUES | CheckerTypes.SUM | CheckerTypes.SEQUENCE | CheckerTypes.PAIRS) -> None:
        self.__dice = dice_values
        self.__min_dice = min_dice
        self.__max_dice = max_dice
        self.__checker_type = checker_type

        if CheckerTypes.PAIRS in self.__checker_type or CheckerTypes.SEQUENCE in self.__checker_type:
            self.__frequencies = Checker.__get_frequencies(self.__dice, self.__min_dice, self.__max_dice)

        if CheckerTypes.SEQUENCE in self.__checker_type:
            self.__max_sequence = Checker.__get_max_sequence(self.__frequencies, self.__min_dice, self.__max_dice)

        if CheckerTypes.PAIRS in self.__checker_type:
            self.__pairs = Checker.__get_pairs(self.__frequencies)
            self.__pairs_lengths = Checker.__get_pairs_lengths(self.__pairs)

        if CheckerTypes.SUM in self.__checker_type:
            self.__dice_sum = sum(self.__dice)
        
        if CheckerTypes.ONLY_VALUES in self.__checker_type:
            self.__dice_set = Checker.__get_dice_values_set(self.__dice)
    
    def _static_init() -> None:
        if Checker.__checker_list is None:
            Checker.__checker_list = Checker.__get_checker_methods()
            Checker.__supported_combinations = Checker.get_supported_combinations()

        if Checker.__Static is None:
            Checker.__Static = Checker.__create_static_class()

    def checker(type : CheckerTypes, name : str):
        def decorator(func : FunctionType):
            func.__dict__['checker'] = {
                'type' : type,
                'name' : name
            }

            @wraps(func)
            def wrapper(*args, **kwargs):
                checker = args[0]

                if type in checker.__checker_type:
                    return func(*args, **kwargs)
                else:
                    raise TypeError(f"Checker type not initialized to use that checker type.")

            return wrapper
        return decorator

#region Properties
    @property
    def dice(self): return self.__dice
    @property
    def min_dice(self): return self.__min_dice
    @property
    def max_dice(self): return self.__max_dice
    @property
    def checker_type(self): return self.__checker_type
#endregion

#region __ Static helper functions
    def __get_frequencies(dice : List[int], min_dice : int, max_dice : int) -> dict:
        return {x : dice.count(x) for x in range(min_dice, max_dice + 1)}

    def __get_max_sequence(dice_frequencies: Dict[int, int], min_dice : int, max_dice : int) -> Tuple[int, int]:
        max_sequence_start = 1
        max_sequence_length = 0
        curr_sequence_length = 0

        for dice_no in range(min_dice, max_dice + 2):
            if dice_no in dice_frequencies and dice_frequencies[dice_no] >= 1:
                curr_sequence_length += 1
            else:
                if curr_sequence_length > max_sequence_length:
                    max_sequence_length = curr_sequence_length
                    max_sequence_start = dice_no - max_sequence_length
                curr_sequence_length = 0
        
        return (max_sequence_start, max_sequence_length)

    def __get_pairs(dice_frequencies: Dict[int, int]) -> List[Tuple[int, int]]:
        return [(dice_no, dice_frequencies[dice_no]) for dice_no in sorted(dice_frequencies.keys()) if dice_frequencies[dice_no] > 1]

    def __get_pairs_lengths(pairs: List[Tuple[int, int]]) -> List[int]:
        return sorted([pair[1] for pair in pairs])
    
    def __get_dice_values_set(dice : List[int]) -> Set[int]:
        return set(dice)

    def __get_checker_methods() -> List[FunctionType]:
        return [func for func in [getattr(Checker, func) for func in dir(Checker)] 
                        if isinstance(func, FunctionType) and "checker" in func.__dict__]

    def __create_static_class() -> Type:
        methods = {}
        for func in Checker.__checker_list:
            methods[func.__name__] = lambda dice,func=func: func.__call__(Checker(dice, checker_type=func.__dict__['checker']['type']))
        
        return type("StaticChecker", (object, ), methods)
#endregion

#region __ Helper checking functions
    def __check_pairs(self, desired_pair_lengths : List[int]) -> bool:
        return self.__pairs_lengths == sorted(desired_pair_lengths)

    def __check_max_sequence(self, desired_length : int) -> bool:
        return self.__max_sequence[1] == desired_length

    def __check_values_only(self, desired_values : Set[int]) -> bool:
        return self.__dice_set.issubset(desired_values)
#endregion

#region _ Static internal functions
    def _get_static_class() -> Type:
        if Checker.__Static is None:
            Checker.__Static = Checker.__create_static_class()
        return Checker.__Static
#endregion

#region Static public functions
    def get_supported_combinations() -> List[str]:
        return [c.__dict__['checker']['name'] for c in Checker.__checker_list]
#endregion

#region Single pairs checking
    @checker(type = CheckerTypes.PAIRS, name = "AAA")
    def check_aaa(self) -> bool:
        return self.__check_pairs([3])

    @checker(type = CheckerTypes.PAIRS, name = "AAAA")
    def check_aaaa(self) -> bool:
        return self.__check_pairs([4])

    @checker(type = CheckerTypes.PAIRS, name = "AAAAA")
    def check_aaaaa(self) -> bool:
        return self.__check_pairs([5])

    @checker(type = CheckerTypes.PAIRS, name = "AAAAAA")
    def check_aaaaaa(self) -> bool:
        return self.__check_pairs([6])
#endregion

#region Double pairs checking
    @checker(type = CheckerTypes.PAIRS, name = "AABB")
    def check_aabb(self) -> bool:
        return self.__check_pairs([2, 2])

    @checker(type = CheckerTypes.PAIRS, name = "AAABB")
    def check_aaabb(self) -> bool:
        return self.__check_pairs([3, 2])

    @checker(type = CheckerTypes.PAIRS, name = "AAABBB")
    def check_aaabbb(self) -> bool:
        return self.__check_pairs([3, 3])

    @checker(type = CheckerTypes.PAIRS, name = "AAAABB")
    def check_aaaabb(self) -> bool:
        return self.__check_pairs([4, 2])
#endregion

#region Triple pairs checking
    @checker(type = CheckerTypes.PAIRS, name = "AABBCC")
    def check_aabbcc(self) -> bool:
        return self.__check_pairs([2, 2, 2])
#endregion

#region Sequence checking
    @checker(type = CheckerTypes.SEQUENCE, name = "ABCD")
    def check_abcd(self) -> bool:
        return self.__check_max_sequence(4)

    @checker(type = CheckerTypes.SEQUENCE, name = "ABCDE")
    def check_abcde(self) -> bool:
        return self.__check_max_sequence(5)
        
    @checker(type = CheckerTypes.SEQUENCE, name = "ABCDEF")
    def check_abcdef(self) -> bool:
        return self.__check_max_sequence(6)
#endregion

#region Sum checking
    @checker(type = CheckerTypes.SUM, name = "<=12")
    def check_lte12(self) -> bool:
        return self.__dice_sum <= 12

    @checker(type = CheckerTypes.SUM, name = ">=30")
    def check_gte30(self) -> bool:
        return self.__dice_sum >= 30
#endregion

#region Only values checking
    @checker(type = CheckerTypes.ONLY_VALUES, name = "=1,3,5")
    def check_e135(self) -> bool:
        return self.__check_values_only({1, 3, 5})
        
    @checker(type = CheckerTypes.ONLY_VALUES, name = "=2,4,6")
    def check_e246(self) -> bool:
        return self.__check_values_only({2, 4, 6})
#endregion

#region Check all
    def check_all(self) -> List[str]:
        return [m.__dict__['checker']['name'] for m in self.__checker_list if m.__call__(self)]
#endregion

Checker._static_init()
StaticChecker = Checker._get_static_class()
