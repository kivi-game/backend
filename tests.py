import unittest
from checker import Checker, StaticChecker

#region Checker tests
class TestCheckerSinglePairs(unittest.TestCase):
    def test_aaa(self):
        self.assertTrue (Checker([1, 3, 5, 1, 1, 2]).check_aaa())

        self.assertFalse(Checker([1, 2, 5, 1, 3, 2]).check_aaa())
        self.assertFalse(Checker([1, 2, 5, 1, 1, 2]).check_aaa())
        
    def test_aaaa(self):
        self.assertTrue (Checker([1, 3, 5, 1, 1, 1]).check_aaaa())
        
        self.assertFalse(Checker([1, 2, 5, 1, 3, 1]).check_aaaa())
        self.assertFalse(Checker([1, 3, 1, 3, 1, 1]).check_aaaa())

    def test_aaaaa(self):
        self.assertTrue (Checker([1, 4, 1, 1, 1, 1]).check_aaaaa())

        self.assertFalse(Checker([1, 2, 3, 1, 1, 1]).check_aaaaa())

    def test_aaaaaa(self):
        self.assertTrue (Checker([3, 3, 3, 3, 3, 3]).check_aaaaaa())
        self.assertFalse(Checker([1, 4, 1, 1, 1, 1]).check_aaaaaa())

class TestCheckerDualPairs  (unittest.TestCase):
    def test_aabb(self):
        self.assertTrue (Checker([1, 4, 2, 4, 3, 1]).check_aabb())

        self.assertFalse(Checker([1, 4, 2, 6, 3, 1]).check_aabb())
        self.assertFalse(Checker([1, 4, 4, 4, 3, 1]).check_aabb())
        self.assertFalse(Checker([1, 4, 4, 4, 1, 1]).check_aabb())
        self.assertFalse(Checker([1, 4, 2, 4, 2, 1]).check_aabb())
        self.assertFalse(Checker([1, 4, 4, 4, 4, 1]).check_aabb())

    def test_aaabb(self):
        self.assertTrue (Checker([1, 4, 2, 4, 4, 1]).check_aaabb())

        self.assertFalse(Checker([1, 4, 4, 2, 1, 5]).check_aaabb())
        self.assertFalse(Checker([1, 4, 4, 4, 1, 1]).check_aaabb())
        self.assertFalse(Checker([1, 4, 4, 4, 4, 1]).check_aaabb())

    def test_aaabbb(self):
        self.assertTrue (Checker([1, 4, 4, 4, 1, 1]).check_aaabbb())

        self.assertFalse(Checker([1, 4, 4, 4, 1, 3]).check_aaabbb())

    def test_aaaabb(self):
        self.assertTrue (Checker([1, 4, 4, 4, 4, 1]).check_aaaabb())

        self.assertFalse(Checker([1, 4, 4, 4, 4, 5]).check_aaaabb())

class TestCheckerTriplePairs(unittest.TestCase):
    def test_aabbcc(self):
        self.assertTrue (Checker([1, 4, 2, 4, 2, 1]).check_aabbcc())

        self.assertFalse(Checker([1, 4, 2, 5, 2, 1]).check_aabbcc())

class TestCheckerSequences  (unittest.TestCase):
    def test_abcd(self):
        self.assertTrue (Checker([2, 4, 5, 3, 2, 4]).check_abcd())
        self.assertTrue (Checker([1, 4, 4, 3, 2, 4]).check_abcd())
        self.assertTrue (Checker([3, 5, 5, 4, 6, 4]).check_abcd())

        self.assertFalse(Checker([2, 4, 5, 6, 2, 4]).check_abcd())
        self.assertFalse(Checker([2, 4, 5, 6, 3, 3]).check_abcd())
        self.assertFalse(Checker([2, 4, 5, 6, 1, 3]).check_abcd())

    def test_abcde(self):
        self.assertTrue (Checker([2, 4, 5, 6, 3, 3]).check_abcde())
        self.assertTrue (Checker([1, 4, 3, 2, 5, 3]).check_abcde())

        self.assertFalse(Checker([2, 4, 5, 6, 1, 4]).check_abcde())
        self.assertFalse(Checker([2, 4, 5, 6, 1, 3]).check_abcde())

    def test_abcdef(self):
        self.assertTrue (Checker([2, 4, 5, 6, 1, 3]).check_abcdef())
        self.assertTrue (Checker([6, 5, 4, 3, 2, 1]).check_abcdef())

        self.assertFalse(Checker([2, 4, 5, 6, 1, 4]).check_abcdef())

class TestCheckerSum        (unittest.TestCase):
    def test_lte12(self):
        self.assertTrue (Checker([1, 3, 2, 1, 2, 1]).check_lte12())
        self.assertTrue (Checker([1, 3, 1, 4, 2, 1]).check_lte12())

        self.assertFalse(Checker([1, 3, 2, 4, 2, 1]).check_lte12())
        self.assertFalse(Checker([4, 5, 4, 3, 2, 1]).check_lte12())

    def test_gte30(self):
        self.assertTrue (Checker([6, 5, 6, 5, 3, 5]).check_gte30())
        self.assertTrue (Checker([6, 5, 6, 5, 5, 5]).check_gte30())

        self.assertFalse(Checker([6, 5, 5, 5, 3, 5]).check_gte30())
        self.assertFalse(Checker([4, 5, 3, 2, 5, 3]).check_gte30())

class TestCheckerOnlyValues (unittest.TestCase):
    def test_e135(self):
        self.assertTrue (Checker([1, 3, 3, 1, 1, 5]).check_e135())
        self.assertTrue (Checker([5, 5, 5, 5, 5, 5]).check_e135())

        self.assertFalse(Checker([1, 3, 3, 4, 5, 3]).check_e135())

    def test_e246(self):
        self.assertTrue (Checker([2, 4, 4, 6, 6, 2]).check_e246())
        self.assertTrue (Checker([4, 4, 4, 4, 4, 4]).check_e246())

        self.assertFalse(Checker([2, 4, 6, 3, 2, 2]).check_e246())
#endregion

#region Static checker tests
class TestStaticCheckerSinglePairs(unittest.TestCase):
    def test_aaa(self):
        self.assertTrue (StaticChecker.check_aaa([1, 3, 5, 1, 1, 2]))

        self.assertFalse(StaticChecker.check_aaa([1, 2, 5, 1, 3, 2]))
        self.assertFalse(StaticChecker.check_aaa([1, 2, 5, 1, 1, 2]))
        
    def test_aaaa(self):
        self.assertTrue (StaticChecker.check_aaaa([1, 3, 5, 1, 1, 1]))
        
        self.assertFalse(StaticChecker.check_aaaa([1, 2, 5, 1, 3, 1]))
        self.assertFalse(StaticChecker.check_aaaa([1, 3, 1, 3, 1, 1]))

    def test_aaaaa(self):
        self.assertTrue (StaticChecker.check_aaaaa([1, 4, 1, 1, 1, 1]))

        self.assertFalse(StaticChecker.check_aaaaa([1, 2, 3, 1, 1, 1]))

    def test_aaaaaa(self):
        self.assertTrue (StaticChecker.check_aaaaaa([3, 3, 3, 3, 3, 3]))
        self.assertFalse(StaticChecker.check_aaaaaa([1, 4, 1, 1, 1, 1]))

class TestStaticCheckerDualPairs  (unittest.TestCase):
    def test_aabb(self):
        self.assertTrue (StaticChecker.check_aabb([1, 4, 2, 4, 3, 1]))

        self.assertFalse(StaticChecker.check_aabb([1, 4, 2, 6, 3, 1]))
        self.assertFalse(StaticChecker.check_aabb([1, 4, 4, 4, 3, 1]))
        self.assertFalse(StaticChecker.check_aabb([1, 4, 4, 4, 1, 1]))
        self.assertFalse(StaticChecker.check_aabb([1, 4, 2, 4, 2, 1]))
        self.assertFalse(StaticChecker.check_aabb([1, 4, 4, 4, 4, 1]))

    def test_aaabb(self):
        self.assertTrue (StaticChecker.check_aaabb([1, 4, 2, 4, 4, 1]))

        self.assertFalse(StaticChecker.check_aaabb([1, 4, 4, 2, 1, 5]))
        self.assertFalse(StaticChecker.check_aaabb([1, 4, 4, 4, 1, 1]))
        self.assertFalse(StaticChecker.check_aaabb([1, 4, 4, 4, 4, 1]))

    def test_aaabbb(self):
        self.assertTrue (StaticChecker.check_aaabbb([1, 4, 4, 4, 1, 1]))

        self.assertFalse(StaticChecker.check_aaabbb([1, 4, 4, 4, 1, 3]))

    def test_aaaabb(self):
        self.assertTrue (StaticChecker.check_aaaabb([1, 4, 4, 4, 4, 1]))

        self.assertFalse(StaticChecker.check_aaaabb([1, 4, 4, 4, 4, 5]))

class TestStaticCheckerTriplePairs(unittest.TestCase):
    def test_aabbcc(self):
        self.assertTrue (StaticChecker.check_aabbcc([1, 4, 2, 4, 2, 1]))

        self.assertFalse(StaticChecker.check_aabbcc([1, 4, 2, 5, 2, 1]))

class TestStaticCheckerSequences  (unittest.TestCase):
    def test_abcd(self):
        self.assertTrue (StaticChecker.check_abcd([2, 4, 5, 3, 2, 4]))
        self.assertTrue (StaticChecker.check_abcd([1, 4, 4, 3, 2, 4]))
        self.assertTrue (StaticChecker.check_abcd([3, 5, 5, 4, 6, 4]))

        self.assertFalse(StaticChecker.check_abcd([2, 4, 5, 6, 2, 4]))
        self.assertFalse(StaticChecker.check_abcd([2, 4, 5, 6, 3, 3]))
        self.assertFalse(StaticChecker.check_abcd([2, 4, 5, 6, 1, 3]))

    def test_abcde(self):
        self.assertTrue (StaticChecker.check_abcde([2, 4, 5, 6, 3, 3]))
        self.assertTrue (StaticChecker.check_abcde([1, 4, 3, 2, 5, 3]))

        self.assertFalse(StaticChecker.check_abcde([2, 4, 5, 6, 1, 4]))
        self.assertFalse(StaticChecker.check_abcde([2, 4, 5, 6, 1, 3]))

    def test_abcdef(self):
        self.assertTrue (StaticChecker.check_abcdef([2, 4, 5, 6, 1, 3]))
        self.assertTrue (StaticChecker.check_abcdef([6, 5, 4, 3, 2, 1]))

        self.assertFalse(StaticChecker.check_abcdef([2, 4, 5, 6, 1, 4]))

class TestStaticCheckerSum        (unittest.TestCase):
    def test_lte12(self):
        self.assertTrue (StaticChecker.check_lte12([1, 3, 2, 1, 2, 1]))
        self.assertTrue (StaticChecker.check_lte12([1, 3, 1, 4, 2, 1]))

        self.assertFalse(StaticChecker.check_lte12([1, 3, 2, 4, 2, 1]))
        self.assertFalse(StaticChecker.check_lte12([4, 5, 4, 3, 2, 1]))

    def test_gte30(self):
        self.assertTrue (StaticChecker.check_gte30([6, 5, 6, 5, 3, 5]))
        self.assertTrue (StaticChecker.check_gte30([6, 5, 6, 5, 5, 5]))

        self.assertFalse(StaticChecker.check_gte30([6, 5, 5, 5, 3, 5]))
        self.assertFalse(StaticChecker.check_gte30([4, 5, 3, 2, 5, 3]))

class TestStaticCheckerOnlyValues (unittest.TestCase):
    def test_e135(self):
        self.assertTrue (StaticChecker.check_e135([1, 3, 3, 1, 1, 5]))
        self.assertTrue (StaticChecker.check_e135([5, 5, 5, 5, 5, 5]))

        self.assertFalse(StaticChecker.check_e135([1, 3, 3, 4, 5, 3]))

    def test_e246(self):
        self.assertTrue (StaticChecker.check_e246([2, 4, 4, 6, 6, 2]))
        self.assertTrue (StaticChecker.check_e246([4, 4, 4, 4, 4, 4]))

        self.assertFalse(StaticChecker.check_e246([2, 4, 6, 3, 2, 2]))
#endregion

if __name__ == '__main__':
    unittest.main()
